from flask import Flask
app = Flask(__name__)
import os

#TODO: add gRPC boilerplate, write simple nodeJS service as client

@app.route("/")
def main():
    return ("Hello, Adi!")


if __name__ == "__main__":
    app.run()
